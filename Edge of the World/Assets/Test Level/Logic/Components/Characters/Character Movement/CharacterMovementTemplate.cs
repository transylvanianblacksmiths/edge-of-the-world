﻿using UnityEngine;

[CreateAssetMenu(fileName = "Character Movement", menuName = "Character/Character Movement", order = 0)]
public class CharacterMovementTemplate : ScriptableObject
{
    public float turnSpeed;
    public float jumpForce;
    public float fallingForwardForce;
}