﻿using UnityEngine;

[CreateAssetMenu(fileName = "UI Text", menuName = "UI/Text", order = 0)]
public class UserInterfaceTextTemplate : ScriptableObject
{
    public Font font;
    public FontStyle fontStyle;
    public int fontSize;
    public int fontMinSize;
    public int fontMaxSize;
    public float lineSpacing;
    public TextAnchor alignment;
    public Color color;
}