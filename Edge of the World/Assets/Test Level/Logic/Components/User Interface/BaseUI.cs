﻿using UnityEngine;

public class BaseUI
{
    protected BaseUI(Canvas canvas)
    {
        Reference = this;
        Canvas = canvas;
    }

    public static BaseUI Reference { get; private set; }
    public Canvas Canvas { get; }
    public bool isVisible { get; set; }
}