﻿using System;
using UnityEngine;

public class GameHud : BaseUI
{
    public GameHud(Canvas canvas, GameObject contextualPromptViewContainer, UserInterfaceTextTemplate contextualPromptViewTextTemplate) : base(canvas)
    {
        ActiveViews = new HeadsUpDisplayTypes[Enum.GetNames(typeof(HeadsUpDisplayTypes)).Length];
        ContextualPromptView = new ContextualPromptView(contextualPromptViewContainer, contextualPromptViewTextTemplate);
    }

    public HeadsUpDisplayTypes[] ActiveViews { get; set; } //TODO to be implemented
    public ContextualPromptView ContextualPromptView { get; set; }
}