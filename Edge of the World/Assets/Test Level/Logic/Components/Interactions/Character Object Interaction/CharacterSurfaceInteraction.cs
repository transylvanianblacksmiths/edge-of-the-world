﻿using System.Collections;
using UnityEngine;

public class CharacterSurfaceInteraction : CharacterObjectInteraction
{
    private static readonly int SleepState = Animator.StringToHash("sleep");
    private static readonly int SitState = Animator.StringToHash("sit");
    private IEnumerator surfaceInteractionRoutine;
    
    public CharacterSurfaceInteraction(MonoBehaviour monoBehaviourRef, ICharacter characterEntity, IInteractable targetInteractableObject) :
        base(monoBehaviourRef, characterEntity, targetInteractableObject)
    {
    }
    
    public override void Initialize()
    {
        targetIKWeight = 0;
        base.Initialize();
    }
    
    public override void StartInteraction()
    {
        switch (targetInteractableObject.ObjectType)
        {
            case InteractableObjectTypes.Bed:
                characterEntity.Animator.SetBool(SleepState, true);
                break;
            case InteractableObjectTypes.Bench:
                characterEntity.Animator.SetBool(SitState, true);
                targetInteractableObject.CurrentActionType = InteractableActionTypes.GetUp;
                break;
        }
    
        base.StartInteraction();
    }
    
    protected override IEnumerator GetInitialPositioningMethod()
    {
        return SetCharacterPosition();
    }
    
    protected override bool GetUseObjectSide()
    {
        return true;
    }
    
    protected override bool IsCharacterOnTheOppositeSide()
    {
        return targetInteractableObject.Transform.InverseTransformPoint(characterEntity.Transform.position).z < 0;
    }
    
    protected override void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject)
    {
        if (targetInteractableObject is null) return;
        switch (eventType)
        {
            case InteractionEventTypes.AnimationExit:
                FinishInteraction();
                break;
            case InteractionEventTypes.CharacterInteraction:
                characterEntity.Animator.SetBool(SitState, false);
                targetInteractableObject.CurrentActionType = InteractableActionTypes.SitDown;
                break;
            default:
                return;
        }
    }
}