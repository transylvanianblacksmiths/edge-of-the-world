﻿using System.Collections;
using Cinemachine;
using UnityEngine;
using static Cinemachine.CinemachineBrain;

public class PlayerDwellerInteraction : ICharacterEnvironmentInteraction
{
    private const float ROTATING_DURATION = 0.3f;
    private const float CAMERA_POSITIONING_DURATION = 0.3f;
    private readonly CinemachineBrain cameraBrain;
    private readonly CinemachineFreeLook cameraController;

    private readonly ICharacter dwellerEntity;
    private readonly MonoBehaviour monoBehaviourRef;
    private readonly ICharacter playerEntity;

    public PlayerDwellerInteraction(MonoBehaviour monoBehaviourRef, ICharacter player, ICharacter dwellerEntity
    )
    {
        this.monoBehaviourRef = monoBehaviourRef;
        this.dwellerEntity = dwellerEntity;
        playerEntity = player;

        cameraController = CameraUtils.CameraController;
        cameraBrain = CameraUtils.GetPlayerCameraBrain();
    }

    public void Initialize()
    {
        CharacterEnvironmentInteractionService.Event += OnInteractionEvent;

        GameObject.FindWithTag("CameraController").GetComponent<CinemachineInputProvider>().enabled = false;
        cameraController.m_YAxis.m_AccelTime = 0;
        cameraController.m_YAxis.m_DecelTime = 0;
        cameraController.m_XAxis.m_AccelTime = 0;
        cameraController.m_XAxis.m_DecelTime = 0;
        CameraUtils.EnableCameraRecenter(monoBehaviourRef, 0, 0.1f);

        monoBehaviourRef.StartCoroutine(SetCameraPosition(-19.1f, 0.16f, 0.95f, new Vector3(0, 0, 0.93f)));

        monoBehaviourRef.StartCoroutine(SetCharacterLookRotation(playerEntity, dwellerEntity));
        monoBehaviourRef.StartCoroutine(SetCharacterLookRotation(dwellerEntity, playerEntity));
    }


    public void FinishInteraction()
    {
        cameraController.m_YAxis.m_AccelTime = 0.3f;
        cameraController.m_YAxis.m_DecelTime = 0.3f;
        cameraController.m_XAxis.m_AccelTime = 0.3f;
        cameraController.m_XAxis.m_DecelTime = 0.3f;

        monoBehaviourRef.StartCoroutine(SetCameraPosition(0, 0.28f, 2.58f, Vector3.zero));
        GameObject.FindWithTag("CameraController").GetComponent<CinemachineInputProvider>().enabled = true;
        CameraUtils.DisableCameraRecenter();
        CharacterEnvironmentInteractionService.Event -= OnInteractionEvent;

        if (playerEntity is IMobile player)
        {
            player.CanRotate = true;
            player.CanMove = true;
            player.CanJump = true;
        }

        dwellerEntity.Collider.enabled = true;
        if (dwellerEntity is IInteractable dweller) dweller.TriggerAreaCollider.enabled = true;

        ((IPlayer) playerEntity).IsInteracting = false;
    }

    public void StartInteraction()
    {
        ((IInteractable) dwellerEntity).CurrentActionType = InteractableActionTypes.Leave;
    }

    public void SetHandPositionIK()
    {
    }

    public void SetHandWeight()
    {
    }

    private void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject)
    {
        switch (eventType)
        {
            case InteractionEventTypes.CharacterInteraction:
                ((IInteractable) dwellerEntity).CurrentActionType = InteractableActionTypes.Talk;
                FinishInteraction();
                break;
            default:
                return;
        }
    }

    private IEnumerator SetCameraPosition(float targetHeadingBias, float targetOrbitHeight, float targetOrbitRadius, Vector3 targetRigOffset)
    {
        cameraBrain.m_UpdateMethod = UpdateMethod.LateUpdate;
        cameraBrain.m_BlendUpdateMethod = BrainUpdateMethod.LateUpdate;
        float interpolatingTimer = 0;

        float startHeadingBias = cameraController.m_Heading.m_Bias;
        float startOrbitHeight = cameraController.m_Orbits[1].m_Height;
        float startOrbitRadius = cameraController.m_Orbits[1].m_Radius;
        Vector3 startRigOffset = cameraController.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset;

        while (interpolatingTimer < CAMERA_POSITIONING_DURATION)
        {
            cameraController.m_Heading.m_Bias = Mathf.Lerp(startHeadingBias, targetHeadingBias, interpolatingTimer / CAMERA_POSITIONING_DURATION);
            cameraController.m_Orbits[1].m_Height = Mathf.Lerp(startOrbitHeight, targetOrbitHeight, interpolatingTimer / CAMERA_POSITIONING_DURATION);
            cameraController.m_Orbits[1].m_Radius = Mathf.Lerp(startOrbitRadius, targetOrbitRadius, interpolatingTimer / CAMERA_POSITIONING_DURATION);
            cameraController.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset =
                Vector3.Lerp(startRigOffset, targetRigOffset, interpolatingTimer / CAMERA_POSITIONING_DURATION);
            interpolatingTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        cameraController.m_Heading.m_Bias = targetHeadingBias;
        cameraController.m_Orbits[1].m_Height = targetOrbitHeight;
        cameraController.m_Orbits[1].m_Radius = targetOrbitRadius;
        cameraController.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset = targetRigOffset;

        cameraBrain.m_UpdateMethod = UpdateMethod.FixedUpdate;
        cameraBrain.m_BlendUpdateMethod = BrainUpdateMethod.FixedUpdate;
    }

    private IEnumerator SetCharacterLookRotation(IEntity character, IEntity target)
    {
        float interpolatingTimer = 0;
        Vector3 characterOrientation = Quaternion
            .LookRotation(
                target.Transform.position - character.Transform.position, Vector3.up).eulerAngles;
        characterOrientation.x = 0;
        characterOrientation.z = 0;
        Quaternion startRotation = character.Transform.rotation;

        StartInteraction();

        while (interpolatingTimer < ROTATING_DURATION)
        {
            character.Transform.rotation =
                Quaternion.Lerp(startRotation, Quaternion.Euler(characterOrientation), interpolatingTimer / ROTATING_DURATION);
            interpolatingTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        character.Transform.rotation = Quaternion.Euler(characterOrientation); //use rigidbody for player
    }
}