﻿using UnityEngine;

public class InteractableLadderController : InteractableObjectController
{
    protected override void Update()
    {
        //TODO add condition when to update
        base.Update();
        SetVisualMarkerPosition();
    }

    protected override IInteractableObject GetInteractableObjectInstance()
    {
        return new InteractableLadder(gameObject, categoryTemplate, objectTemplate, handPosition, handRotation);
    }

    protected override Vector3 GetVisualMarkerOffset()
    {    
        return playerEntity.Transform.position.y > interactableObjectEntity.ObjectCenter.y
            ? Vector3.zero
            : base.GetVisualMarkerOffset();
    }
}