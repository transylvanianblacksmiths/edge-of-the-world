﻿public class InteractableRestingSurfaceController : InteractableObjectController
{
    protected override IInteractableObject GetInteractableObjectInstance()
    {
        return new InteractableRestingSurface(gameObject, categoryTemplate, objectTemplate, handPosition, handRotation);
    }
}