﻿using UnityEngine;

public class InteractableRestingSurface : InteractableObject
{
    public InteractableRestingSurface(GameObject gameObjectRef, InteractableCategoryTemplate interactableCategoryTemplate,
        InteractableObjectTemplate objectTemplate, Vector3 handPosition, Vector3 handRotation) : base(gameObjectRef, interactableCategoryTemplate,
        objectTemplate, handPosition, handRotation)
    {
    }
}