﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DebugController : MonoBehaviour
{    //TODO disable ui navigation
    public Button menuButton;
    public GameObject systemInfo;
    public GameObject tools;
    public GameObject debugMenu;
    public Button respawnItemsButton;

    private PlayerInputControls inputControls;
    private bool isMenuVisible;
    private bool isMouseVisible;
    private bool isRespawningItems;
    private ICharacter playerEntity;

    private void Start()
    {
        playerEntity = GameUtils.GetPlayerEntity();

        StartCoroutine(RespawnItems());
        if (!Debug.isDebugBuild)
        {
            gameObject.SetActive(false);
            return;
        }

        SetButtonListeners();
        SetInputActions();
        DebugService.Event += OnDebugEvent;
    }

    private void Update()
    {
        respawnItemsButton.interactable = !((IPlayer)playerEntity).IsInteracting && !isRespawningItems;
    }

    private void OnDestroy()
    {
        DebugService.Event -= OnDebugEvent;
    }

    private void SetButtonListeners()
    {
        menuButton.onClick.AddListener(() =>
        {
            isMenuVisible = !isMenuVisible;
            debugMenu.SetActive(isMenuVisible);
        });
    }

    private void SetInputActions()
    {
        inputControls = InputService.inputControls;
        inputControls.Debug.ShowMouse.performed += _ => SetMouseVisible();
    }

    private void OnDebugEvent(DebugEventTypes eventType, bool value)
    {
        switch (eventType)
        {
            case DebugEventTypes.ExitGame:
                Application.Quit();
                break;
            case DebugEventTypes.ShowSystemInfo:
                systemInfo.SetActive(value);
                break;
            case DebugEventTypes.ShowTools:
                tools.SetActive(value);
                break;
            case DebugEventTypes.RespawnItems:
                StartCoroutine(RespawnItems());
                break;
        }
    }

    private void SetMouseVisible()
    {
        isMouseVisible = !isMouseVisible;
        Cursor.visible = isMouseVisible;
    }
    
    private IEnumerator RespawnItems()
    {//TODO make generic function in utils
        isRespawningItems = true;
        if (SceneManager.GetSceneByBuildIndex(1).IsValid())
        {
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(1);
            while (!asyncUnload.isDone) yield return null;
        }

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);

        while (!asyncLoad.isDone) yield return null;
        isRespawningItems = false;
    }
}