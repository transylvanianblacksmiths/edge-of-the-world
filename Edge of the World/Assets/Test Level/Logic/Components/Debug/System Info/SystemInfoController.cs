using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;

public class SystemInfoController : MonoBehaviour
{
    public Text deviceType;
    public Text deviceName;
    public Text operatingSystem;
    public Text cpuType;
    public Text cpuCount;
    public Text cpuFrequency;
    public Text cpuThreadMode;
    public Text gpuType;
    public Text gpuVersion;
    public Text gpuMultiThreaded;
    public Text gpuShaderLevel;
    public Text gpuMemoryUsage;
    public Text frameRate;
    public Text memorySize;
    public Text memoryAllocated;
    private IEnumerator fpsCounterRoutine;

    private float frameTime;
    private float totalGpuMemory;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        frameTime += (Time.unscaledDeltaTime - frameTime) * 0.1f;
    }

    private void FixedUpdate()
    {
        SetAllocatedMemoryValue();
        SetGraphicsMemoryValue();
    }

    private void OnEnable()
    {
        if (!Debug.isDebugBuild)
        {
            return;
        }

        SetSystemInfoValues();
        fpsCounterRoutine = ShowFPSCounter();
        StartCoroutine(fpsCounterRoutine);
    }

    private void OnDisable()
    {
        StopCoroutine(fpsCounterRoutine);
    }

    private void SetSystemInfoValues()
    {
        //Device Info
        deviceType.text = SystemInfo.deviceType.ToString();
        deviceName.text = SystemInfo.deviceName;
        operatingSystem.text = SystemInfo.operatingSystem;

        //CPU Info
        cpuType.text = SystemInfo.processorType;
        cpuCount.text = SystemInfo.processorCount.ToString();
        cpuFrequency.text = SystemInfo.processorFrequency / 1000.0f + " GHz";
        cpuThreadMode.text = SystemInfo.renderingThreadingMode.ToString();

        //GPU Info
        gpuType.text = SystemInfo.graphicsDeviceName;
        gpuVersion.text = SystemInfo.graphicsDeviceVersion;
        gpuMultiThreaded.text = SystemInfo.graphicsMultiThreaded.ToString();
        gpuShaderLevel.text = SystemInfo.graphicsShaderLevel.ToString();
        totalGpuMemory = SystemInfo.graphicsMemorySize;

        //Memory Info
        memorySize.text = SystemInfo.systemMemorySize / 1000.0f + " GB";
    }

    private IEnumerator ShowFPSCounter()
    {
        while (Debug.isDebugBuild)
        {
            float fps = 1.0f / frameTime;
            string text = $"{fps:0.} ({frameTime * 1000.0f:0.0} ms)";
            frameRate.text = text;

            if (fps < 30 && fps > 10)
                frameRate.color = Color.yellow;
            else if (fps <= 10)
                frameRate.color = Color.red;
            else
                frameRate.color = Color.white;

            yield return new WaitForSecondsRealtime(0.5f);
        }
    }

    private void SetAllocatedMemoryValue()
    {
        long usedMemory = Profiler.GetTotalAllocatedMemoryLong() / 1000000;
        long totalMemory = Profiler.GetTotalReservedMemoryLong() / 1000000;

        memoryAllocated.text = usedMemory + " MB / " + totalMemory + " MB";
    }

    private void SetGraphicsMemoryValue()
    {
        float allocatedMemory = Profiler.GetAllocatedMemoryForGraphicsDriver() / 1000000.0f;
        int allocatedMemoryRatio = Convert.ToInt32(allocatedMemory / totalGpuMemory * 100);

        gpuMemoryUsage.text = allocatedMemoryRatio + "%   " + $"{Profiler.GetAllocatedMemoryForGraphicsDriver() / 1000000:n0}" + " MB / " +
                              $"{totalGpuMemory:n0}" + " MB";
    }
}