﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class CreateGameComponent : ScriptableWizard
{    
    public enum ScriptTypes
    {
        Entity,
        Controller,
        Repository,
        Service,
        Template
    }

    public string componentName;
    public List<ScriptTypes> structure = new List<ScriptTypes> {ScriptTypes.Entity, ScriptTypes.Controller};

    private void OnEnable()
    {
        minSize = new Vector2(500, 400);
        maxSize = new Vector2(500, 400);
        helpString = "Component name has to be written in UpperCamelCase (ex. TestComponent).";
    }

    private void OnWizardCreate()
    {
        string parentFolderName = Regex.Replace(componentName, @"(\p{Lu})", " $1").TrimStart();
        string parentFolder = AssetDatabase.CreateFolder(AssetDatabase.GetAssetPath(Selection.activeObject.GetInstanceID()), parentFolderName);
        string parentFolderPath = AssetDatabase.GUIDToAssetPath(parentFolder);

        foreach (ScriptTypes script in structure)
        {
            string scriptTypeName = Enum.GetName(typeof(ScriptTypes), script);
            string filePath = Application.dataPath.Replace("Assets", "") + parentFolderPath;
            string scriptName = componentName;
            
            if (script != ScriptTypes.Entity)
                scriptName += scriptTypeName;
            GenerateScript(filePath + "/" + scriptName + ".cs", $"public class {scriptName}\n{{\n \n}}");
        }

        AssetDatabase.Refresh();
    }

    private void OnWizardUpdate()
    {
        if (structure.Count != structure.Distinct().Count())
        {
            errorString = "Component structure contains duplicates.";
            isValid = false;
        }
        else if (componentName is null || componentName.Length == 0)
        {
            errorString = "Component name is empty.";
            isValid = false;
        }
        else
        {
            errorString = "";
            isValid = true;
        }
    }

    [MenuItem("Assets/Create/Project/Game Component", true, -1)]
    private static bool NewMenuOptionValidation()
    {
        Object obj = Selection.activeObject;
        if (obj == null) return false;
        string path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
        return path.Length > 0 && Directory.Exists(path);
    }

    [MenuItem("Assets/Create/Project/Game Component", false, -1)]
    private static void CreateWizard()
    {
        DisplayWizard("Create Game Component", typeof(CreateGameComponent), "Create");
    }

    private static void GenerateScript(string path, string template)
    {
        try
        {
            using FileStream fileStream = File.Create(path);
            byte[] scriptContent = new UTF8Encoding(true).GetBytes(template);
            fileStream.Write(scriptContent, 0, scriptContent.Length);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }
}