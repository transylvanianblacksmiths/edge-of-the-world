﻿using UnityEngine;

public interface ICharacter : IEntity
{
    public Animator Animator { get; }
    public AudioSource AudioSource { get; }
    public ICharacterSoundGroup[] SoundGroup { get; }
}