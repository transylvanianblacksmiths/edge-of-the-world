﻿public interface IMobile
{
    public float AirTime { get; set; }
    public float DistanceToGround { get; set; }
    public bool IsGrounded { get; set; }
    public bool IsRunning { get; set; }
    public bool IsCrouching { get; set; }
    public bool CanRotate { get; set; }
    public bool CanMove { get; set; }
    public bool CanRun { get; set; }
    public bool CanJump { get; set; }
}