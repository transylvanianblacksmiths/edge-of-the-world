﻿public interface IController
{
    public T GetEntity<T>();
}
