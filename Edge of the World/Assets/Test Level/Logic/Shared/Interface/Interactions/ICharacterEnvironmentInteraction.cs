﻿using JetBrains.Annotations;

public interface ICharacterEnvironmentInteraction
{
    public void Initialize();

    public void FinishInteraction();

    [UsedImplicitly]
    void StartInteraction();
    void SetHandPositionIK();
    void SetHandWeight();
}