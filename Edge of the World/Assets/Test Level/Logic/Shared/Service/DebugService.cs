﻿using System;

public static class DebugService
{
    public static event Action<DebugEventTypes, bool> Event;

    public static void NotifyEvent(DebugEventTypes eventType, bool value = false)
    {
        Event?.Invoke(eventType, value);
    }
}