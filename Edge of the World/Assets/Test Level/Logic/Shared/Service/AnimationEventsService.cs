﻿using System;

public static class AnimationEventsService
{
    public static event Action<AnimationEventTypes, ICharacter> Event;

    public static void NotifyEvent(AnimationEventTypes eventType, ICharacter character)
    {
        Event?.Invoke(eventType, character);
    }
}