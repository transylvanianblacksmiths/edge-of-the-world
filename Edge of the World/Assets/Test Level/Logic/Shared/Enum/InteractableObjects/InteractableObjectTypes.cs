﻿using System;
using JetBrains.Annotations;

[Serializable]
[UsedImplicitly]
public enum InteractableObjectTypes
{
    GroundItem,
    SurfaceItem,
    Bed,
    Bench,
    Door,
    MovableObject,
    Ladder
}