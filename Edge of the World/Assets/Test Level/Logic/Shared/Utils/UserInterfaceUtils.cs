﻿using UnityEngine;

public static class UserInterfaceUtils
{
    public static T CreateUIGameObject<T>(string name, RectTransform parent, Vector3 position, Vector2 size)
    {
        GameObject uiGameObject = new GameObject(name, typeof(RectTransform));

        RectTransform transform = uiGameObject.GetComponent<RectTransform>();
        transform.SetParent(parent, false);
        transform.localPosition = position;
        transform.sizeDelta = size;

        uiGameObject.AddComponent(typeof(T));
        return uiGameObject.GetComponent<T>();
    }
}